import java.util.Scanner;
public class Shop{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Burger[] burgers = new Burger[4];
		for(int i = 0; i < burgers.length; i++){
			System.out.println((i+1) + " out of " + burgers.length + " entries");
			burgers[i] = new Burger();
			System.out.print("Please enter the burger price: ");
			burgers[i].price = reader.nextDouble();
			System.out.print("Please enter the meat you want to have in your burger: ");
			burgers[i].meatType = reader.next();
			System.out.print("Would you like extra cheese in your burger (yes/no): ");
			burgers[i].extraCheese = reader.next();
			System.out.println("");
		}
		Burger lastBurger = burgers[burgers.length - 1];
		System.out.println("");
		System.out.println("The price of the burger is " + lastBurger.price + "$");
		System.out.println("The meat that you chose for you burger is " + lastBurger.meatType);
		if (lastBurger.extraCheese.equals("yes")){
			System.out.println("You chose extra cheese for your burger");
		}
		else {
			System.out.println("You chose to pass on extra cheese, how dare you...");
		}
		System.out.println("Do you consider burgers as essential food? (yes/no)");
		String essentialFood = reader.next();
		if (lastBurger.ApplyTax(essentialFood, lastBurger.price)){
			System.out.println("Your burger was taxed :(");
			System.out.println("The new price is " + lastBurger.price + "$");
		}
		else{
			System.out.println("Fine, we won't tax your burger...");
		}
	}
}
